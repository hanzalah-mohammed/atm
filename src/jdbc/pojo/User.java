package jdbc.pojo;

public class User {

    private String userEmail;
    private double bankBalance;

    public void insertData(String userEmail, double bankBalance){
        this.userEmail = userEmail;
        this.bankBalance = bankBalance;
    }

    public String getUserEmail() {
        return userEmail;
    }
    public double getBankBalance() {
        return bankBalance;
    }
    public void setBankBalance(double bankBalance) {
        this.bankBalance = bankBalance;
    }
}
