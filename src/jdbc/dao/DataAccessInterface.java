package jdbc.dao;

public interface DataAccessInterface {

    // for transactions
    void checkBalance();
    void depositAmount();
    void withdrawAmount();

    // for login authentication
    boolean authenticateUser(String userEmail, String userPassword);

    // for register new user
    boolean checkExistingUser(String userEmail);
    void createNewUser(String userEmail, String userPassword, String securityKey);

    // for reset password
    boolean checkSecurityKey(String userEmail, String securityKey);
    void resetPassword(String newPassword, String newSecurityKey, String userEmail);
}
