package jdbc.dao;

import jdbc.pojo.*;
import utility.DBUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class DataAccessImplementation implements DataAccessInterface{

    User refUser;
    Scanner refScanner = new Scanner(System.in);
    Connection refConnection = DBUtility.getConnection();
    PreparedStatement ps = null;
    ResultSet result = null;

    @Override
    public boolean checkExistingUser(String userEmail) {
        try {
            String sqlQuery = "SELECT userEmail FROM user WHERE userEmail = ?";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, userEmail); // set the first ? as userEmail
            result = ps.executeQuery(); // Execute the query

            // Get the result from the query and store them in variables
            result.next();
            String resultEmail = result.getString("userEmail");

            // Authenticate the user inputs with the result variables
            if (userEmail.equals(resultEmail)) {
                return true;
            }
        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            return false;
        }
        return false;
    }

    @Override
    public void createNewUser(String userEmail, String userPassword, String securityKey) {

        try {
            String sqlQuery = "INSERT into user(userEmail, userPassword, securityKey, bankBalance) " +
                    "VALUES (?, ?, ?, ?)";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, userEmail); // set the first ? as userEmail
            ps.setString(2, userPassword);
            ps.setString(3, securityKey);
            ps.setDouble(4, 0);
            ps.execute(); // Execute the query

        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            System.err.println("Operation failed!");
        }
    }

    @Override
    public boolean authenticateUser(String userEmail, String userPassword) {

        try {
            String sqlQuery = "SELECT * FROM user WHERE userEmail = ?";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, userEmail); // set the first ? as userEmail
            result = ps.executeQuery(); // Execute the query

            // Get the result from the query and store them in variables
            result.next();
            String resultEmail = result.getString("userEmail");
            String resultPassword = result.getString("userPassword");
            double resultBalance = result.getDouble("bankBalance");

            // Authenticate the user inputs with the result variables
            if (userEmail.equals(resultEmail) && userPassword.equals(resultPassword)) {
                refUser = new User();
                refUser.insertData(resultEmail, resultBalance);
                return true;
            } else { // if the password does not match
                throw new Exception("Login Failed! Password is incorrect. \n");
            }
        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            System.err.println("Login Failed! User '" + userEmail + "' does not exist. \n");
        } catch (Exception e) { // catch the thrown Exception
            System.err.println(e.getMessage());
        } // end of try and catch
        return false;
    }

    @Override
    public void checkBalance(){
        System.out.println("Your bank balance is $" + refUser.getBankBalance());
    }

    @Override
    public void depositAmount(){
        System.out.println("Enter Amount to deposit:");
        double amount = refScanner.nextDouble();

        if(amount < 0){
            System.out.println("Amount can't be negative!!");
        } else {
            refUser.setBankBalance(refUser.getBankBalance() + amount);
            System.out.println("$" + amount + " deposited successfully!");
            System.out.println("Your new balance is $" + refUser.getBankBalance());
            updateBankBalance();
        }
    }

    @Override
    public void withdrawAmount(){
        System.out.println("Enter Amount to withdraw:");
        double amount = refScanner.nextDouble();

        if(amount > refUser.getBankBalance()){
            System.out.println("Sorry!! insufficient balance.");
        } else {
            refUser.setBankBalance(refUser.getBankBalance() - amount);
            System.out.println("Transaction successful!");
            System.out.println("Your new balance is $" + refUser.getBankBalance());
            updateBankBalance();
        }
    }

    public void updateBankBalance() {

        try {
            String sqlQuery = "UPDATE user SET bankBalance = ? where userEmail = ?";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setDouble(1, refUser.getBankBalance()); // set the first ? as userEmail
            ps.setString(2, refUser.getUserEmail());
            ps.execute(); // Execute the query

        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            System.err.println("Operation failed!");
        }
    }

    @Override
    public boolean checkSecurityKey(String userEmail, String securityKey) {
        try {
            String sqlQuery = "SELECT userEmail, securityKey FROM user WHERE userEmail = ?";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, userEmail); // set the first ? as userEmail
            result = ps.executeQuery(); // Execute the query

            // Get the result from the query and store them in variables
            result.next();
            String resultKey = result.getString("securityKey");

            // Authenticate the user inputs with the result variables
            if (securityKey.equals(resultKey)) {
                return true;
            }
        } catch (SQLException e) { // if the SQL cannot run due to invalid input
            return false;
        }
        return false;
    }

    @Override
    public void resetPassword(String newPassword, String newSecurityKey, String userEmail) {

        try {
            String sqlQuery = "UPDATE user SET userPassword = ?, securityKey = ? where userEmail = ?";

            ps = refConnection.prepareStatement(sqlQuery); // take the sqlQuery
            ps.setString(1, newPassword); // set the first ? as newPassword
            ps.setString(2, newSecurityKey);
            ps.setString(3, userEmail);
            int rowAffected = ps.executeUpdate(); // Execute the query

            if( rowAffected > 0){
                System.out.println(" Your Password has been successfully reset!");
            } else {
                throw new Exception();
            }

        } catch (Exception e) { // if the SQL cannot run due to invalid input
            System.err.println("Operation failed!");
        }
    }
}


