package jdbc.controller;

import jdbc.service.AtmInterface;
import jdbc.service.AtmImplementation;

public class AtmController {

    AtmInterface refInterface = new AtmImplementation(); // initialize AtmInterface reference, all methods will access the same User object

    public void callRegisterService(){
        refInterface.userRegistration(); // call userRegistration in AtmImplementation
    }

    public void callLoginService(){
        refInterface.userLogin(); // call userLogin in AtmImplementation
    }

    public void callResetService() {
        refInterface.resetPassword(); // call resetPassword in AtmImplementation
    }
}
